<?php

class Color
{
    private $red;
    private $green;
    private $blue;

    /**
     * init Class
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @throws Exception
     */
    public function __construct(int $red, int $green, int $blue)
    {
        try {
            $this->setRed($red);
            $this->setGreen($green);
            $this->setBlue($blue);
        } catch (Exception $e) {
            print $e->getMessage();
            die();
        }
    }

    /**
     * Set red color
     *
     * @param $color
     * @throws Exception
     * @return void
     */
    private function setRed($color): void
    {
        if(!$this->isCorrectColor($color))
            throw new Exception('not correct number color');

        $this->red = $color;
    }

    /**
     * Set red green
     *
     * @param $color
     * @throws Exception
     * @return void
     */
    private function setGreen($color): void
    {
        if(!$this->isCorrectColor($color))
            throw new Exception('not correct number color', 0);

        $this->green = $color;
    }

    /**
     * Set blue color
     *
     * @param $color
     * @throws Exception
     * @return void
     */
    private function setBlue($color): void
    {
        if(!$this->isCorrectColor($color))
            throw new Exception('not correct number color', 0);

        $this->blue = $color;
    }

    /**
     * Get red color
     *
     * @return int
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * Get green color
     *
     * @return int
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * Get blue color
     *
     * @return int
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * Get is equals colors
     *
     * @param self $model
     * @return bool
     */
    public function isEquals(self $model): bool
    {
        // Checking red color on the equal
        if(!$this->isEqual($this->getRed(), $model->getRed()))
            return false;

        // Checking red color on the equal
        if(!$this->isEqual($this->getGreen(), $model->getGreen()))
            return false;

        // Checking red color on the equal
        if(!$this->isEqual($this->getBlue(), $model->getBlue()))
            return false;

        return true;
    }

    /**
     * checkin correct color
     *
     * @param int $color
     * @return bool
     */
    private function isCorrectColor(int $color): bool
    {
        if($color < 1 || $color > 255)
            return false;
        return true;
    }

    /**
     * Get is equal two colors
     *
     * @param int $firstColor
     * @param int $secondColor
     * @return bool
     */
    private function isEqual(int $firstColor, int $secondColor): bool 
    {
        if($firstColor === $secondColor)
            return true;
        return false;
    }

    public function mix(self $model): void
    {
        $mixRed = $this->getRed() + $model->getRed();
        $mixGreen = $this->getGreen() + $model->getGreen();
        $mixBlue = $this->getBlue() + $model->getBlue();

        $this->setRed($mixRed / 2);
        $this->setGreen($mixGreen / 2);
        $this->setBlue($mixBlue / 2);
    }

    /**
     * Get random Color
     *
     * @return static
     * @throws Exception
     */
    public static function random(): self
    {
        return new self(rand(1, 255), rand(1, 255), rand(1, 255));
    }
}