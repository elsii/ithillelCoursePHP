<?php
require_once ('model/Color.php');

$color = new Color(200, 200, 200);

print PHP_EOL. '========= Equals ===========' . PHP_EOL;
var_dump($color->isEquals(new Color(100, 100, 100)));
print PHP_EOL. '========= Mix ===========' . PHP_EOL;

$color->mix(new Color(100, 100, 100));
var_dump($color);

print PHP_EOL. '========== Random ==========' . PHP_EOL;
var_dump(Color::random());
